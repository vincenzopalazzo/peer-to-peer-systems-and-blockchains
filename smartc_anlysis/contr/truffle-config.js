module.exports = {
  // Uncommenting the defaults below 
  // provides for an easier quick-start with Ganache.
  // You can also follow this format for other networks;
  // see <http://truffleframework.com/docs/advanced/configuration>
  // for more details on how to specify configuration options!
  compilers: {
    solc: {
        version: "0.8.1",  // ex:  "0.4.20". (Default: Truffle's installed solc)
        optimizer: {
            enabled: true,
            runs: 500
        }
    },
  },
  mocha: {
      reporter: 'eth-gas-reporter',
      reporterOptions : {
          gasPrice: "wei"
      }
  }
  //networks: {
  //  development: {
  //    host: "127.0.0.1",
  //    port: 7545,
  //    network_id: "*"
  //  },
  //  test: {
  //    host: "127.0.0.1",
  //    port: 7545,
  //    network_id: "*"
  //  }
  //}
  //
};
