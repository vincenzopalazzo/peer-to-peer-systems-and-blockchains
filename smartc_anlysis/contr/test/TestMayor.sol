// SPDX-License-Identifier: MIT
pragma solidity 0.8.1;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";

import "../contracts/Mayor.sol";

contract TestMayor {

    /// This is only a simple test to make me comfortable
    /// with the test framework.
    function testRundMapComputeEnvelop() public {
        Mayor mayor = Mayor(DeployedAddresses.Mayor());
        bytes32 expected = keccak256(abi.encode(2000, true, 10000));
        Assert.equal(mayor.compute_envelope(2000, true, 10000), expected,
                     "Envelop is compute in the wrong way");
    }
}
