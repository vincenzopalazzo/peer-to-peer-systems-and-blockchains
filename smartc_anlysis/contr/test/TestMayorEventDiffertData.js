const truffleAssert = require('truffle-assertions');

const Mayor = artifacts.require("Mayor");

contract("Mayor", (accounts) => {
    let listAccount = accounts;

    it("Make a envelop cast", async () => {
        const instance = await Mayor.new(listAccount[0], listAccount[1], listAccount.length - 2);
        const envelops = await instance.compute_envelope(100, true, 2000);
        const result = await instance.cast_envelope(envelops);
        truffleAssert.eventEmitted(result, "EnvelopeCast");
    });

    it("Open Envelope without reaching quorum", async () => {
        const instance = await Mayor.new(listAccount[0], listAccount[1], listAccount.length - 2);
        const envelops = await instance.compute_envelope(100, true, 2000);
        await instance.cast_envelope(envelops);
        await truffleAssert.fails(
            instance.open_envelope.call(100, true, {value: 2000}));
    });

    it("Envelop Open after reaching quorum", async () => {
        const instance = await Mayor.new(listAccount[0], listAccount[1], listAccount.length - 2);
        for (let i = 0; i < listAccount.length - 2; i++) {
            const envelops = await instance.compute_envelope(10 + i, i % 2, (i + 1) * 1000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }
        const result = await instance.open_envelope.sendTransaction(10, 0 % 2,
                                                        {from: listAccount[2],
                                                         value: 1000});
        truffleAssert.eventEmitted(result, "EnvelopeOpen");
    });

    it("Open Envelop and NewMayor calculation", async () => {
        const instance = await Mayor.new(listAccount[0], listAccount[1], listAccount.length - 2);
        for (let i = 0; i < listAccount.length - 2; i++) {
            const envelops = await instance.compute_envelope(10 + i, true, (i + 1) * 1000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }

        for (let i = 0; i < listAccount.length - 2; i++) {
            await instance.open_envelope
                .sendTransaction(10 + i, true,
                                 {from: listAccount[i + 2],
                                  value: (i + 1) * 1000});
        }
        const result = await instance.mayor_or_sayonara();
        truffleAssert.eventEmitted(result, "NewMayor");
    });


    it("Open Envelop and Sayonara calculation", async () => {
        const instance = await Mayor.new(listAccount[0], listAccount[1], listAccount.length - 2);
        for (let i = 0; i < listAccount.length - 2; i++) {
            const envelops = await instance.compute_envelope(10 + i, false, (i + 1) * 1000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }

        for (let i = 0; i < listAccount.length - 2; i++) {
            await instance.open_envelope
                .sendTransaction(10 + i, false,
                                 {from: listAccount[i + 2],
                                  value: (i + 1) * 1000});
        }
        const result = await instance.mayor_or_sayonara();
        truffleAssert.eventEmitted(result, "Sayonara");
    });

    it("Open Envelop and Sayonara calculation", async () => {
        const instance = await Mayor.new(listAccount[0], listAccount[1], listAccount.length - 2);
        for (let i = 0; i < listAccount.length - 2; i++) {
            const envelops = await instance.compute_envelope(10 + i, false, (i + 1) * 1000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }

        for (let i = 0; i < listAccount.length - 2; i++) {
            await instance.open_envelope
                .sendTransaction(10 + i, false,
                                 {from: listAccount[i + 2],
                                  value: (i + 1) * 1000});
        }
        const result = await instance.mayor_or_sayonara();
        truffleAssert.eventEmitted(result, "Sayonara");
        await truffleAssert.fails(instance.mayor_or_sayonara());
    });

});
