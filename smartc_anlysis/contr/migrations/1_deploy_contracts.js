const Mayor = artifacts.require("Mayor");
const MayorQuorum = artifacts.require("MayorQuorum");

module.exports = function(deployer, network, accounts) {
    deployer.deploy(Mayor, accounts[0], accounts[1], 5);
    deployer.deploy(MayorQuorum, accounts[0], accounts[1], 5);
};
