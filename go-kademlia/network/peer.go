package network

import (
	"github.com/vincenzopalazzo/go-kademlia/node"
)

type PeerNetwork struct {
	networkConfig *NetworkConfig
	nodeConfig    *node.NetworkNode
}
