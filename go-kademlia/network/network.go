package network

import (
	"errors"
	"github.com/anacrolix/utp"
	"github.com/vincenzopalazzo/go-kademlia/node"
	"net"
	"sync"
)

// Terminology used inside the networking people?
// Stun: It is a standard definition protocol described here https://www.ietf.org/rfc/rfc3489.txt
//
type NetworkConfig struct {
	// UTP socket it is a improvment of the TCP protocol
	// on Bit torrent network.
	// https://www.bittorrent.org/beps/bep_0029.html
	node          *node.NetworkNode
	socket        *utp.Socket
	address       *net.UDPAddr
	connection    *net.UDPConn
	lock          *sync.Mutex
	aliveConns    *sync.WaitGroup
	status        *status
	remoteAddress string
}

type status struct {
	connected   bool
	initialized bool
}

func InitConnection(node *node.NetworkNode) *NetworkConfig {
	return &NetworkConfig{node: node, lock: &sync.Mutex{}, status: &status{false, true}}
}

func (config *NetworkConfig) MakeSocket(host string, port string, withStun bool, stunAddr string) (publicHost string, publicPort string, err error) {
	config.lock.Lock()
	defer config.lock.Unlock()
	if config.status.connected {
		return "", "", errors.New("Already connected")
	}

	// Create socket
	config.remoteAddress = "[" + host + "]" + ":" + port

	socket, err := utp.NewSocket("udp", config.remoteAddress)
	if err != nil {
		return "", "", err
	}

	if withStun {
		//TODO configure Stun protocol with the library utp
	}

	config.status.connected = true
	config.socket = socket
	return host, port, nil
}
