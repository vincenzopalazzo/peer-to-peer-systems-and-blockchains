package dht

import (
	"time"
	"error"
)

type DHashTable struct {
	//node: Is the node of the hash table.
	// the reference is needed because each change
	// (if any) need to be reflected also here.
	node *NetworkNode
	//TODO: Refactoring the routing table with a better
	//datastructure.
	routingTable [][]*NetworkNode

	// lock: The table can be under different request at the same time
	// and we need to ha some method to fix the mutual access at this
	// variable.
	lock *sync.Mutex

	rules *rules
}

// The Kademlia has some rules to respect and
// to avoit the avriable spread around the file
// we put all the rules inside this structure.

type rules struct {
	// the size in bits of the keys used to identify nodes and store and
	// retrieve data; in basic Kademlia this is 160, the length of a SHA1
	refreshMap [160]time.Time
}

// TODO: Working here
func MakeDHashTable(options *Options) (*DHashTable, error) {
	return nil, nil
}
