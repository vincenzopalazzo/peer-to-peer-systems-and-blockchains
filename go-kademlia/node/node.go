package node

import (
	"math/big"
	"net"
)

// Network node rapresentation as described int the original network.
type NetworkNode struct {
	NodeId   []byte
	NodeIp   net.IP
	NodePort uint64
}

// Function that perform the XOR distance from another node.
func (fromNode NetworkNode) GetDistance(toNode NetworkNode) (distance *big.Int) {
	valueFromNode := new(big.Int).SetBytes(fromNode.NodeId)
	valueToNode := new(big.Int).SetBytes(toNode.NodeId)
	distance = new(big.Int).Xor(valueFromNode, valueToNode)
	return
}
