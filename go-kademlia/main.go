package main

import (
	"fmt"
	"github.com/akamensky/argparse"
	log "github.com/sirupsen/logrus"
	"github.com/vincenzopalazzo/go-kademlia/network"
	"github.com/vincenzopalazzo/go-kademlia/node"
	"github.com/vincenzopalazzo/go-kademlia/utils"
	"net"
	"os"
	"strconv"
)

func init() {
	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)
}

func makeNetworkNode(ipFlag string, portFlag string) (*node.NetworkNode, error) {
	nodeIp := net.ParseIP(ipFlag)
	nodePort, err := strconv.ParseUint(portFlag, 32, 64)
	if err != nil {
		return nil, err
	}
	log.Debug("Node Ip parsered: ")
	log.Debug(nodeIp)
	nodeId := utils.MakeSha1(ipFlag + portFlag) // TODO: very bad seed
	nodeRes := node.NetworkNode{nodeId, nodeIp, nodePort}
	return &nodeRes, nil
}

func main() {
	parser := argparse.NewParser("go-kademlia", "Implementation of the Kademlia Protocol in Go lang")

	version := parser.Flag("v", "version", &argparse.Options{Required: false, Default: false, Help: "Print version application"})
	ipFlag := parser.String("i", "ip", &argparse.Options{Required: false, Help: "Set the IP address where make the binding"})
	portFlag := parser.String("p", "port", &argparse.Options{Required: false, Help: "Set the port where make the binding"})

	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
		return
	}

	if *version {
		fmt.Println("Version App: Go-Kademlia v0.0.1")
		return
	}

	if ipFlag == nil || portFlag == nil {
		fmt.Println("Ip or port missed")
		return
	}
	log.Debug("Ip flag " + *ipFlag + " port flag: " + *portFlag)
	nodeNetwork, err := makeNetworkNode(*ipFlag, *portFlag)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	networkConf := network.InitConnection(nodeNetwork)
	hostIp, hostPort, err := networkConf.MakeSocket(*ipFlag, *portFlag, false, "")
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	fmt.Println("Listen socket on " + hostIp + ":" + hostPort)
	return
}
