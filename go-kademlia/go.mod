module github.com/vincenzopalazzo/go-kademlia

go 1.15

require (
	github.com/akamensky/argparse v1.2.2
	github.com/anacrolix/envpprof v1.1.1 // indirect
	github.com/anacrolix/sync v0.2.0 // indirect
	github.com/anacrolix/utp v0.1.0
	github.com/bradfitz/iter v0.0.0-20191230175014-e8f45d346db8 // indirect
	github.com/ccding/go-stun/stun v0.0.0-20200514191101-4dc67bcdb029
	github.com/devfacet/gocmd v3.1.0+incompatible // indirect
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/jbenet/go-base58 v0.0.0-20150317085156-6237cf65f3a6
	github.com/nsf/gocode v0.0.0-20190302080247-5bee97b48836 // indirect
	github.com/prettymuchbryce/kademlia v0.0.0-20200622200022-b7ff38c776c4
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4 // indirect
	gopkg.in/readline.v1 v1.0.0-20160726135117-62c6fe619375
)
