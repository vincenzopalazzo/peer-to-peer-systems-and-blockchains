package utils

import (
	"crypto/sha1"
)

func MakeSha1(value string) (bytesHash []byte) {
	hashFunction := sha1.New()
	hashFunction.Write([]byte(value))
	hashHex := hashFunction.Sum(nil)
	return hashHex
}
