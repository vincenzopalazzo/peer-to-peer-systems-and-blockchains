import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#263238',
        },
        secondary: {
            main: '#32424A',
        },
        background: {
            default: '#263238',
            paper: '#263238',
        },
        text: {
            primary: '#607D8B',
        },
        divider: '#009688',
    },
});

export default theme