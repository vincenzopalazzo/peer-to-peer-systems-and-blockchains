import IPFS from 'ipfs-core'

let _SINGLETON;

class IpfsUtils {

    static getInstance() {
        if (_SINGLETON === undefined) {
            _SINGLETON = new IpfsUtils();
        }
        return _SINGLETON;
    }

    constructor() {
        this.ipfs = undefined;
    }

    async initIpfs(){
        if (!this.ipfs) {
            this.ipfs = await IPFS.create();
        }
    }

    async getVersion() {
        return await this.ipfs.version();
    }

    async getPeersInfo() {
        return await this.ipfs.swarm.addrs()
    }

    async expandPeerInformation(peerId) {
        return await this.ipfs.id(peerId);
    }
}

export default IpfsUtils