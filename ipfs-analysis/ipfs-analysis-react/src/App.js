import {Component} from 'react'
import {
    AppBar,
    Toolbar,
    IconButton,
    Typography,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell, TableBody, Paper,
    Container, Box, Button
} from '@material-ui/core'
import {Menu} from '@material-ui/icons'
import Drawer from "./components/Drawer";
import IpfsUtils from './utils/IpfsUtils'

class App extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            drawerOpen: false,
            version: 0,
            peers: []
        }
    }

    async getVersion() {
        let version = await IpfsUtils.getInstance().getVersion();
        this.setState({
            version: version
        })
    }

    async getPeers() {
        let peers = await IpfsUtils.getInstance().getPeersInfo();
        console.debug(peers)
        this.setState({
            peers: peers
        });
    }

    async componentDidMount() {
        await IpfsUtils.getInstance().initIpfs();
        await this.getVersion();
        await this.getPeers();
    }

    render() {
        // Crash app, Why? <Drawer open={this.state.drawerOpen} handleDrawerClose={()=> this.setState({drawerOpen: !this.state.drawerOpen})}/>
        return (
            <Container maxWidth="xl">
                <AppBar position="sticky">
                    <Toolbar>
                        <IconButton onClick={()=> this.setState({drawerOpen: true})} edge="start" color="inherit" aria-label="menu">
                            <Menu/>
                        </IconButton>
                        <Typography variant="h6">
                            IPFS Peer Analysis IPFS Version {this.state?.version.version}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Box my={4}>
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Id Pees</TableCell>
                                    <TableCell>Address</TableCell>
                                    <TableCell>Expand</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.peers.map((row) => (
                                    <TableRow key={row.id}>
                                        <TableCell component="th" scope="row">
                                            {row.id}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {row.addrs[0].toString()}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            <Button>Expand</Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>
            </Container>
        );
    }
}

export default App;
