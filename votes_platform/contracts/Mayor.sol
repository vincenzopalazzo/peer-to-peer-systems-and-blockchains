// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

contract Mayor {
    // Structs, events, and modifiers
    // Store refund data
    struct Refund {
        uint soul;
    }

    // Data to manage the confirmation
    struct Conditions {
        uint32 quorum;
        uint32 envelopes_casted;
        uint32 envelopes_opened;
    }

    struct CandidateInfo {
        // Semplify the work in the mayonara function
        address id;
        uint candidate_soul; // How much the candidate will rewars
        uint32 voters;
        uint voters_val;
        address[] voters_list;
        bool exist;
    }

    event NewMayor(address _candidate);
    event Sayonara(address _escrow);
    event NewDeposit(address _candidate, uint _tot_val);
    event EnvelopeCast(address _voter);
    event EnvelopeOpen(address _voter, uint _soul, address _candidate);
    event Debug(string _event, uint _value);

    // Someone can vote as long as the quorum is not reached
    modifier canVote() {
        require(voting_condition.envelopes_casted < voting_condition.quorum,
                "Cannot vote now, voting quorum has been reached");
        _;
    }

    // Envelopes can be opened only after receiving the quorum
    modifier canOpen() {
        require(voting_condition.envelopes_casted == voting_condition.quorum,
                "Cannot open an envelope, voting quorum not reached yet");
        _;
    }

    // The outcome of the confirmation can be computed as soon as all the casted envelopes have been opened
    modifier canCheckOutcome() {
        require(voting_condition.envelopes_opened == voting_condition.quorum,
                "Cannot check the winner, need to open all the sent envelopes");
        _;
    }

    modifier reentrancyLock() {
        require(locked == false, "The voting session is ended");
        _;
    }

    modifier alreadyVote() {
        require(hasVote[msg.sender] == false, "The sender has already express a vote");
        _;
    }

    modifier isCandidate() {
        require(candidates[msg.sender].exist == true, "The sender is not a candidate");
        _;
    }

    // State attributes
    // Initialization variables
    mapping(address => CandidateInfo) candidates;
    address payable public escrow;

    // Voting phase variables
    mapping(address => bytes32) envelopes;

    Conditions voting_condition;

    bool public locked = false;

    // Refund phase variables
    mapping(address => Refund) souls;
    // This need to be included inside the Candidate info?
    // A voter can vote for different candidate?
    mapping(address => bool) hasVote;
    /*
     * Increasing the length of a storage array by calling push() has constant gas costs because storage is zero-initialised,
     * while decreasing the length by calling pop() has a cost that depends on the “size” of the element being removed.
     * If that element is an array, it can be very costly, because it includes explicitly clearing the removed elements similar
     * to calling delete on them.
     */
    address[] candidates_addr;

    /// @notice The constructor only initializes internal variables
    /// @param _candidates (address) The address of the mayor candidate
    /// @param _escrow (address) The address of the escrow account
    /// @param _quorum (address) The number of voters required to finalize the confirmation
    constructor(address[] memory _candidates, address payable _escrow, uint32 _quorum) {
        require(_candidates.length > 0, "At list one candidate is required");
        for (uint32 i = 0; i < _candidates.length; i++) {
            address key = _candidates[i];
            address[] memory _voter_list = new address[](0);
            candidates[key] = CandidateInfo({id: key, candidate_soul: 0, voters: 0, voters_val: 0, voters_list: _voter_list, exist: true});
            candidates_addr.push(key);
        }
        escrow = _escrow;
        voting_condition = Conditions({quorum: _quorum, envelopes_casted: 0, envelopes_opened: 0});
    }

    /// @notice Store a received voting envelope
    /// @param _envelope The envelope represented as the keccak256 hash of (sigil, doblon, soul)
    function cast_envelope(bytes32 _envelope) canVote public {

        if(envelopes[msg.sender] == 0x0) // => NEW, update on 17/05/2021
            voting_condition.envelopes_casted++;

        envelopes[msg.sender] = _envelope;
        emit EnvelopeCast(msg.sender);
    }

    /// @notice Open an envelope and store the vote information
    /// @param _sigil (uint) The secret sigil of a voter
    /// @param _candidate (address) The address of the candidate
    /// @dev The soul is sent as crypto
    /// @dev Need to recompute the hash to validate the envelope previously casted
    function open_envelope(uint _sigil, address _candidate) alreadyVote canOpen public payable {

        require(envelopes[msg.sender] != 0x0,
                "The sender has not casted any votes");
        require(candidates[_candidate].exist == true, "The address choosen is not inside the candidate list");

        bytes32 _casted_envelope = envelopes[msg.sender];
        bytes32 _sent_envelope = compute_envelope(_sigil, _candidate, msg.value);

        require(_casted_envelope == _sent_envelope,
                "Sent envelope does not correspond to the one casted.");


        CandidateInfo storage info = candidates[_candidate];
        info.voters++;
        info.voters_val += msg.value;
        info.voters_list.push(msg.sender);

        souls[msg.sender] = Refund(msg.value);

        voting_condition.envelopes_opened++;
        hasVote[msg.sender] = true;
        emit EnvelopeOpen(msg.sender, msg.value, _candidate);
    }

    /// @notice Either confirm or kick out the candidate. Refund the electors who voted for the losing outcome
    // After all envelopes have been opened, the smart contract checks
    // whether the mayor is confirmed or not: in any case, the voters who expressed the “losing
    // vote” (nay in case of confirmation, yay in case of rejection) get their soul back as refund.
    function mayor_or_sayonara() canCheckOutcome reentrancyLock public {
        locked = true;
        (CandidateInfo memory candidate_info, bool winners) = compute_winner();
        require(candidate_info.exist == true, "The address choosen is not inside the candidate list");
        if (winners) {
            bool result = compute_calculation_winner(candidate_info);
            if (result)
                emit NewMayor(candidate_info.id);
        } else {
            bool result = compute_escrow();
            if (result)
                emit Sayonara(escrow);
        }
    }

    function deposit_eth_candidate() isCandidate public payable {
        candidates[msg.sender].candidate_soul += msg.value;
        emit NewDeposit(msg.sender, candidates[msg.sender].candidate_soul);
    }

    // TODO: use a sort algorithm to check the condition here
    // not implemented here because we need a more generic language
    // https://github.com/ethereum/solidity/issues/869
    function compute_winner() private returns (CandidateInfo memory, bool) {
        locked = true;
        address _candidate = candidates_addr[0];
        uint _max = candidates[_candidate].voters_val;
        for (uint32 i = 1; i < candidates_addr.length; i++) {
            address tmp = candidates_addr[i];
            CandidateInfo memory tmpInfo = candidates[tmp];
            if (tmpInfo.voters_val > _max) {
                _max = tmpInfo.voters_val;
                _candidate = tmp;
            }
        }

        for (uint32 i = 0; i < candidates_addr.length; i++) {
            address tmp = candidates_addr[i];
            CandidateInfo memory tmpInfo = candidates[tmp];
            if (tmpInfo.voters_val == _max && tmpInfo.id != _candidate) {
                 CandidateInfo memory _actual_winner = candidates[_candidate];
                if (tmpInfo.voters > _actual_winner.voters) {
                    return (tmpInfo, true); 
                } else if (tmpInfo.voters == _actual_winner.voters) {
                    return (candidates[_candidate], false); // The draw between candate all to escrow address
                } else {
                    return (candidates[_candidate], true);
                }
            }
        }
        // There is a clear winner
        return (candidates[_candidate], true);
    }

    function compute_escrow() private returns (bool) {
        uint _accumulate = 0;
        for (uint32 i = 0; i < candidates_addr.length; i++) {
            address tmp = candidates_addr[i];
            CandidateInfo memory tmpInfo = candidates[tmp];
            for (uint32 j = 0; j < tmpInfo.voters_list.length; j++) {
                address _voter = tmpInfo.voters_list[j];
                _accumulate += souls[_voter].soul;
            }
        }
        bool result = escrow.send(_accumulate);
        emit Debug("escrow", _accumulate);
        require(result == true, "Fails payament in the escrow");
        return true;
    }

    function compute_calculation_winner(CandidateInfo memory _candidate) private returns (bool){
        locked = true;
        uint rewars = uint(_candidate.candidate_soul / _candidate.voters);
        for (uint32 i = 0; i < _candidate.voters; i++) {
            address payable userAddr = payable(_candidate.voters_list[i]);
            if (rewars > 0) {
                bool result = userAddr.send(rewars);
                require(result == true, "Fails payament in the rewars");
            }
        }

        uint rewars_for_candidate = 0;

        for (uint32 i = 0; i < candidates_addr.length; i++) {
            address _tmp = candidates_addr[i];
            if (_tmp != _candidate.id) {
                CandidateInfo memory _info = candidates[_tmp];
                rewars_for_candidate += _info.candidate_soul;
            }
        }

        if (rewars_for_candidate > 0) {
            address payable _addr = payable(_candidate.id);
            bool result = _addr.send(rewars_for_candidate);
            emit Debug("rewars_for_candidate", rewars_for_candidate);
            require(result == true, "Fails payament in the rewars_for_candidate");
        }

        for (uint32 i = 0; i < candidates_addr.length; i++) {
            if (candidates_addr[i] != _candidate.id) {
                CandidateInfo memory tmp = candidates[candidates_addr[i]];
                for (uint32 j = 0; j < tmp.voters_list.length; j++) {
                    address payable userAddr = payable(_candidate.voters_list[i]);
                    Refund memory user = souls[userAddr];
                    if (user.soul > 0) {
                        bool result = userAddr.send(user.soul);
                        emit Debug("candidates_addr", user.soul);
                        require(result == true, "Fails payament in the candidates_addr");
                    }
                }
            }
        }
        return true;
    }

    /// @notice Compute a voting envelope
    /// @param _sigil (uint) The secret sigil of a voter
    /// @param _candidate (address) The address of the candidate
    /// @param _soul (uint) The soul associated to the vote
    function compute_envelope(uint _sigil, address _candidate, uint _soul) public pure returns(bytes32) {
        return keccak256(abi.encode(_sigil, _candidate, _soul));
    }

    function get_candidates() public view returns (address[] memory) {
        return candidates_addr;
    }

    function get_candidate_soul(address _addr) public view returns (uint) {
        return candidates[_addr].candidate_soul;
    }

    function get_condition() public view returns (uint32, uint32, uint32) {
        return (voting_condition.quorum, voting_condition.envelopes_casted, voting_condition.envelopes_opened);
    }

    function check_has_voted(address _address) public view returns(bool) {
        return hasVote[_address];
    }
}
