const truffleAssert = require('truffle-assertions');

const Mayor = artifacts.require("Mayor");

contract("Mayor", (accounts) => {
    let listAccount = accounts;
    let candidates = [];
    let instance;
    
    beforeEach(async () => {
        candidates = [];
        for (let i = 0; i < 2; i++) {
            candidates.push(accounts[i]);
        }
        instance = await Mayor.new(candidates, listAccount[2], 6);
    })

    it("Make a envelop cast", async () => {
        const envelops = await instance.compute_envelope(100, listAccount[3], 1000000000000);
        const result = await instance.cast_envelope(envelops);
        truffleAssert.eventEmitted(result, "EnvelopeCast");
    });

    it("Open Envelope without reaching quorum", async () => {
        const envelops = await instance.compute_envelope(100, listAccount[3], 1000000000000);
        await instance.cast_envelope(envelops);
        await truffleAssert.fails(
            instance.open_envelope.call(100, listAccount[3], {value: 1000000000000}));
    });

    it("Envelop Open after reaching quorum", async () => {
        for (let i = 0; i < 6; i++) {
            const envelops = await instance.compute_envelope(10 + i, listAccount[1], (i + 1) * 1000000000000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }
        const result = await instance.open_envelope.sendTransaction(10, listAccount[1],
                                                        {from: listAccount[2],
                                                         value: 1000000000000});
        truffleAssert.eventEmitted(result, "EnvelopeOpen");
    });

    it("Open Envelop and Sayonara calculation", async () => {
        for (let i = 0; i < 6; i++) {
            const index = i % 2;
            const envelops = await instance.compute_envelope(10 + i, listAccount[index], 1000000000000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }

        for (let i = 0; i < 6; i++) {
            const index = i % 2;
            await instance.open_envelope
                .sendTransaction(10 + i, listAccount[index],
                                 {from: listAccount[i + 2],
                                  value: 1000000000000});
        }
        const result = await instance.mayor_or_sayonara();
        truffleAssert.eventEmitted(result, "Sayonara");
    });

    it("Open Envelop and NewMayor calculation with more soul", async () => {
        for (let i = 0; i < 6; i++) {
            const index = i % 2;
            const envelops = await instance.compute_envelope(10 + i, listAccount[index], (i + 1) * 1000000000000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }

        for (let i = 0; i < 6; i++) {
            const index = i % 2;
            await instance.open_envelope
                .sendTransaction(10 + i, listAccount[index],
                                 {from: listAccount[i + 2],
                                  value: (i + 1) * 1000000000000});
        }
        await instance.deposit_eth_candidate.sendTransaction({from: listAccount[0], value: 1000000000000});
        await instance.deposit_eth_candidate.sendTransaction({from: listAccount[1], value: 1000000000000});
        const result = await instance.mayor_or_sayonara();
        truffleAssert.eventEmitted(result, "NewMayor");
    });
    
    it("Candidate deposit", async () => {
        const result = await instance.deposit_eth_candidate.sendTransaction({from: listAccount[0], value: 1000000000000});
        truffleAssert.eventEmitted(result, "NewDeposit");
    });

    it("Open Envelop and NewMayor calculation with more votes", async () => {
        for (let i = 0; i < 6; i++) {
            const envelops = await instance.compute_envelope(10 + i, listAccount[1], 1000000000000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }

        for (let i = 0; i < 6; i++) {
            await instance.open_envelope
                .sendTransaction(10 + i, listAccount[1],
                                 {from: listAccount[i + 2],
                                  value: 1000000000000});
        }
        await instance.deposit_eth_candidate.sendTransaction({from: listAccount[0], value: 1000000000000});
        await instance.deposit_eth_candidate.sendTransaction({from: listAccount[1], value: 1000000000000});
        const result = await instance.mayor_or_sayonara();
        truffleAssert.eventEmitted(result, "NewMayor");
    });

    it("Fixed reentrancy attack", async () => {
        for (let i = 0; i < 6; i++) {
            const envelops = await instance.compute_envelope(10 + i, listAccount[1], 1000000000000);
            await instance.cast_envelope(envelops,
                                         {from: listAccount[i + 2]});
        }

        for (let i = 0; i < 6; i++) {
            await instance.open_envelope
                .sendTransaction(10 + i, listAccount[1],
                                 {from: listAccount[i + 2],
                                  value: 1000000000000});
        }
        await instance.deposit_eth_candidate.sendTransaction({from: listAccount[0], value: 1000000000000});
        await instance.deposit_eth_candidate.sendTransaction({from: listAccount[1], value: 1000000000000});
        const result = await instance.mayor_or_sayonara();
        truffleAssert.eventEmitted(result, "NewMayor");
        await truffleAssert.fails(instance.mayor_or_sayonara());
    });
});
