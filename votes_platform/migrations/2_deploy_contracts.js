var Mayor = artifacts.require("./Mayor.sol");

module.exports = function(deployer, network, accounts) {
  let candidates = [];
  for (let i = 0; i < 3; i++) {
    candidates.push(accounts[i]);
  }
  deployer.deploy(Mayor, candidates, accounts[3], 1);
};
