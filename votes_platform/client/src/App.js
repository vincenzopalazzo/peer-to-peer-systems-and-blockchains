import React, {Component} from "react";

import {Container} from "@material-ui/core";
import BasicAppBar from './components/BasicAppBar.component'
import Home from './components/Home.component'
import ContractUtils from "./utils/ContractUtils";
import CandidateViewComponent from "./components/CandidateView.component";
import EventQueue from "./components/EventQueue.component";
import WinnerView from "./components/WinnerView.component";
import AppModel from "./utils/AppModel";
import {QUORUM_COND, VOTERS_COND, ENVELOP_OPENED} from "./utils/Constant";

const PagesEnum = {"HOME":"home", "EVENTS": "events", "WINNER": "winner"}
Object.freeze(PagesEnum)

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            storageValue: 0,
            drawerOpen: false,
            web3Ready: false,
            isCandidate: false,
            page: <Home/>,
            pageName: PagesEnum.HOME,
            admin: <CandidateViewComponent />,
            quorum: 1,
            voters: 0
        };
        this.changePage = this.changePage.bind(this);
        this.updateState = this.updateState.bind(this);
    }

    async updateState() {
        const constrains = await ContractUtils.getInstance().getConditionQuorum();
        AppModel.getInstance().putValue(QUORUM_COND, constrains[0]);
        AppModel.getInstance().putValue(VOTERS_COND, constrains[1]);
        AppModel.getInstance().putValue(ENVELOP_OPENED, constrains[2]);
        this.setState({quorum: constrains[0], voters: constrains[1]});
    }

    async componentDidMount() {
        try {
            await ContractUtils.getInstance().initWeb3()
            const isCandidate = await ContractUtils.getInstance().isCandidate()
            const constrains = await ContractUtils.getInstance().getConditionQuorum();
            AppModel.getInstance().putValue(QUORUM_COND, constrains[0]);
            AppModel.getInstance().putValue(VOTERS_COND, constrains[1]);
            this.setState({web3Ready: true, isCandidate: isCandidate, quorum: constrains[0], voters: constrains[1]});
            ContractUtils.getInstance().subscribeToEnvelopeCastEvent(this.updateState);
            ContractUtils.getInstance().subscribeToEnvelopeOpenEvent(this.updateState);
        }catch (e) {
            console.error(e);
        }
    }

    changePage(value) {
        let page;
        let pageName;
        console.log("Value is: ", value)
        switch (value) {
            case "home":
                page = <Home/>
                pageName = PagesEnum.HOME
                break
            case "events":
                page = <EventQueue/>
                pageName = PagesEnum.EVENTS
                break
            case "winner":
                page = <WinnerView/>
                pageName = PagesEnum.WINNER
                break
            default:
                throw new Error("Error page not exist")
        }
        this.setState({
            page: page,
            pageName: pageName
        })
    }


    render() {
       if (!this.state.web3Ready) {
            return <div>Loading Web3, accounts, and contract...</div>;
        }
        return (
            <Container maxWidth="xl">
                <BasicAppBar
                    value={this.state.pageName}
                    changeValue={this.changePage}
                    title="Democratic P2P Valadilène"
                    voters={this.state.voters}
                    quorum={this.state.quorum}
                >
                    {this.state.isCandidate ? this.state.admin : this.state.page}
                </BasicAppBar>
            </Container>
        );
    }
}

export default App;
