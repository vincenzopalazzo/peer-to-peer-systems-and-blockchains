import React from 'react';
import AppModel from "../utils/AppModel";
import {QUORUM_COND, VOTERS_COND, ENVELOP_OPENED, VOTING_FINISHED} from "../utils/Constant";
import {Box, Button, Grid, Snackbar} from "@material-ui/core";
import {Alert, AlertTitle} from "@material-ui/lab";
import ContractUtils from "../utils/ContractUtils";

class WinnerView extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            quorum: AppModel.getInstance().getValue(QUORUM_COND, 1),
            voters: AppModel.getInstance().getValue(VOTERS_COND, 0),
            envelopOpened: AppModel.getInstance().getValue(ENVELOP_OPENED, 0),
            completed: AppModel.getInstance().getValue(VOTING_FINISHED,  false),
            snackbar: {
                visible: false,
                message: "",
            },
        }

        this.callMayorOrSayonara = this.callMayorOrSayonara.bind(this);
    }

    callMayorOrSayonara() {
        ContractUtils.getInstance().mayorOrSayonara().then(() => {
            this.showSnackBar(true, "Computing voters calculations");
        });
        ContractUtils.getInstance().subscribeToSayonaraEvent(() => this.setState({completed: true}))
        ContractUtils.getInstance().subscribeToNewMayorEvent(() => this.setState({completed: true}))
        AppModel.getInstance().putValue(VOTING_FINISHED, true);
    }

    showSnackBar(visible, message) {
        this.setState({snackbar: {visible, message}})
    }

    render() {
        if (this.state.completed) {
            return <Alert variant="outlined" severity="warning">
                The Votation is finished, consult the notify panel to get the result
            </Alert>
        }
        return <>
            <Grid container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  spacing={3}>
                <Button variant="outlined"
                        size="large"
                        color="primary"
                        disabled={this.state.envelopOpened < this.state.voters}
                        onClick={this.callMayorOrSayonara}
                        style={{borderRadius: "50%", width: "10em", height: "10em"}}
                >
                    Conclude
                </Button>

                {this.state.envelopOpened < this.state.voters && <Grid item xs={12}>
                    <Box component="span" m={5}>
                        <Alert variant="outlined" severity="warning">
                            <AlertTitle>Action Required</AlertTitle>
                            The quorum is reached but not all the envelop are opened!
                        </Alert>
                    </Box>
                </Grid>
                }
            </Grid>
            <Snackbar
                open={this.state.snackbar.visible}
                autoHideDuration={6000}
                message={this.state.snackbar.message}
                action={
                    <Button color="inherit" size="small" onClick={() => this.showSnackBar(false, "")}>
                        Go it
                    </Button>
                }
            />
        </>
    }

}

export default WinnerView;