import React from 'react'
import {Grid} from "@material-ui/core";
import CandidateCard from "./CandidateCard.component";
import AppModel from "../utils/AppModel";
import {ACCOUNT_VOTE, ACCOUNT_HAS_VOTE} from "../utils/Constant";

class CandidatesView extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {
            accountHasVote: AppModel.getInstance().getValue(ACCOUNT_VOTE) === undefined ? false : AppModel.getInstance().getValue(ACCOUNT_VOTE),
            hasVoteAddress: AppModel.getInstance().getValue(ACCOUNT_HAS_VOTE) === "" ? false : AppModel.getInstance().getValue(ACCOUNT_HAS_VOTE),
        }

        this.setCandidateHasVote = this.setCandidateHasVote.bind(this);
        this.setVotedAddress = this.setVotedAddress.bind(this);
    }

    setCandidateHasVote(value) {
        AppModel.getInstance().putValue(ACCOUNT_VOTE, value)
        this.setState({accountHasVote: value})
    }

    setVotedAddress(address) {
        AppModel.getInstance().putValue(ACCOUNT_HAS_VOTE, address)
        this.setState({hasVoteAddress: address})
    }

    render() {
        return <>
            <Grid
                container
                spacing={5}
                alignItems="center"
            >
                { this.props.candidates.map((candidate, index) =>
                    <Grid item key={index} xs={4}>
                        <CandidateCard
                            voters={this.props.voters}
                            quorum={this.props.quorum}
                            hasVoteAddress={this.state.hasVoteAddress}
                            setVoterAddress={this.setVotedAddress}
                            accountHasVote={this.state.accountHasVote}
                            setAccountHasVote={this.setCandidateHasVote}
                            snackbar={this.props.snackbar}
                            candidate={candidate} />
                    </Grid>
                ) }
            </Grid>
        </>
    }
}

export default CandidatesView;