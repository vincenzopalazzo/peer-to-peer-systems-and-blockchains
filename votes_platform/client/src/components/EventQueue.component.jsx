import React from "react";
import {Card, CardContent, Grid, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import {CallMade, CallReceived} from "@material-ui/icons";
import AppModel from "../utils/AppModel";
import {EVENT_LIST} from "../utils/Constant";

class EventQueue extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            events: AppModel.getInstance().getValue(EVENT_LIST, new Set()),
        }
    }

    render() {
        return <>
            <Grid container
                  direction="column"
                  justify="center"
                  spacing={3}>
                <Card alignItems="center" style={{margin: 50}}>
                    <CardContent>
                        <List alignItems="center">
                            {Array.from(this.state.events, ([_, val]) => val).map((event, index) => <EventItem item={event} key={index}/>)}
                        </List>
                    </CardContent>
                </Card>
            </Grid>
        </>
    }
}

/**
 * Internal component to use inside the event list
 */
class EventItem extends React.Component {
    render() {
        let {item, key} = this.props;
        return <>
            <ListItem key={key}>
                <ListItemIcon>
                    {item.inputOrOutput ? <CallReceived/> : <CallMade/>}
                </ListItemIcon>
                <ListItemText
                    primary={item.name}
                    secondary={item.inputOrOutput ? "Received" : "Sent"}
                />
            </ListItem>
        </>
    }
}

export default EventQueue;