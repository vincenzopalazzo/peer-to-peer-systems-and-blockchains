import React from 'react'
import {
    Avatar, Button,
    Card,
    CardActions,
    CardContent,
    CardHeader, Dialog, DialogActions, DialogContent, Grid,
    IconButton, TextField,
    Typography
} from "@material-ui/core";
import {Favorite} from "@material-ui/icons";
import QRCode from 'qrcode.react'
import ContractUtils from "../utils/ContractUtils";

class CandidateCard extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            openDialog: false,
            openInputDialog: false,
            valueToSend: 0,
            addressSelected: "",
            sentences: ["“A computer once beat me at chess, but it was no match for me at kick boxing.”\n" +
            "— Emo Philips", "“Computer Science is no more about computers than astronomy is about telescopes.”\n" +
            "— Edsger W. Dijkstra", "“The computer was born to solve problems that did not exist before.”\n" +
            "— Bill Gates", "“Standards are always out of date.  That’s what makes them standards.”\n" +
            "— Alan Bennett", "“Imagination is more important than knowledge.  For knowledge is limited, whereas imagination embraces the entire world, stimulating progress, giving birth to evolution.”\n" +
            "— Albert Einstein", "“Where is the ‘any’ key?”\n" +
            "— Homer Simpson”", "“The Internet?  We are not interested in it.”\n" +
            "— Bill Gates, 1993"],
            sentence: undefined,
            iconIndex: -1,
        }

        this.openDialog = this.openDialog.bind(this);
        this.setValueAndClose = this.setValueAndClose.bind(this);
        this.onChangeTextValue = this.onChangeTextValue.bind(this);
        this.sendVote = this.sendVote.bind(this);
    }

    openDialog(value) {
        this.setState({openDialog: value});
    }

    setValueAndClose(close) {
        this.setState({openInputDialog: close});
    }

    onChangeTextValue(event) {
        this.setState({valueToSend: event.target.value})
    }

    sendVote(event) {
        ContractUtils.getInstance().sendVote(this.props.candidate, this.state.valueToSend)
            .then((result) => {
                console.log("Vote sent and return the following result " + result);
                this.props.snackbar(true, "Vote Sent");
                this.props.setAccountHasVote(true);
                this.props.setVoterAddress(this.props.candidate);
            })
            .catch((error) => {
                this.props.snackbar(true, error.toString());
                console.error(error);
            });
        this.setValueAndClose(false);
        event.preventDefault();
    }

    render() {
        if (!this.state.sentence) {
            //TODO(vincenzopalazzo) Refactoring this logic
            let sentence = this.state.sentences[Math.floor(Math.random() * (this.state.sentences.length - 1))];
            let iconIndex = Math.floor(Math.random() * 4)
            this.setState({sentence, iconIndex});
            return null;
        }
        return <>
            <Card style={{width: 350, height: 300}}>
                <CardHeader
                    avatar={
                        <Avatar
                            style={{
                                width: 70,
                                height: 70,
                            }}
                            aria-label="recipe">
                            <img alt="" style={{width: 70, height: 70}}
                                 src={`icons/avatar-${this.state.iconIndex}.gif`}/>
                        </Avatar>
                    }
                    action={
                        <IconButton
                            onClick={() => this.openDialog(!this.state.openDialog)}
                            aria-label="settings">
                            <img alt="" style={{width: 50, height: 50}} src="icons/qr-code.svg"/>
                        </IconButton>
                    }
                    title={this.state.sentence.split("—")[1]}
                >
                    {this.props.candidate === this.props.hasVoteAddress &&
                    <img alt="" style={{width: 50, height: 50}} src="icons/checkmark.svg"/>
                    }
                </CardHeader>
                <CardContent style={{width: 350, height: 120}}>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {this.state.sentence}
                    </Typography>
                </CardContent>
                <CardActions style={{justifyContent: "center"}}>
                    <Button
                        style={{width: "70%"}}
                        color="primary"
                        size="large"
                        variant="outlined"
                        disabled={this.props.quorum === this.props.voters}
                        startIcon={<Favorite/>}
                        onClick={() => this.setValueAndClose(true)}
                    > Vote </Button>
                </CardActions>
            </Card>
            <Dialog
                open={this.state.openDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogContent>
                    <Grid
                        container
                        spacing={2}
                        alignItems="center"
                    >
                        <Grid item xs={2}/>
                        <Grid item xs={6}>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {this.props.candidate}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}/>

                        <Grid item xs={12}/>

                        <Grid item xs={4}/>
                        <Grid item xs={4}>
                            <QRCode value={this.props.candidate}/>
                        </Grid>
                        <Grid item xs={4}/>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.openDialog(!this.state.openDialog)} color="primary">
                        Close
                    </Button>
                </DialogActions>
            </Dialog>

            <Dialog
                open={this.state.openInputDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <form noValidate autoComplete="off" onSubmit={this.sendVote}>

                    <DialogContent>
                        <Grid
                            container
                            spacing={2}
                            direction="column"
                            alignItems="center"
                            justify="center"
                        >
                            <TextField id="outlined-basic"
                                       label="Souls"
                                       variant="outlined"
                                       value={this.state.valueToSend}
                                       onChange={this.onChangeTextValue}
                            />
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={this.sendVote}>
                            Send
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </>
    }
}

export default CandidateCard;