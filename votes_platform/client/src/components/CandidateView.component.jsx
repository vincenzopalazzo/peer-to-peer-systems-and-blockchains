import React from "react"
import {Avatar, Button, Container, Grid, Snackbar, TextField, Typography} from "@material-ui/core";
import ContractUtils from "../utils/ContractUtils";
import theme from "../utils/Theme";
import {AccountBalanceWallet} from "@material-ui/icons";

class CandidateViewComponent extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            randomIndex: Math.floor(Math.random() * 4),
            sentences: ["“A computer once beat me at chess, but it was no match for me at kick boxing.”\n" +
            "— Emo Philips", "“Computer Science is no more about computers than astronomy is about telescopes.”\n" +
            "— Edsger W. Dijkstra", "“The computer was born to solve problems that did not exist before.”\n" +
            "— Bill Gates", "“Standards are always out of date.  That’s what makes them standards.”\n" +
            "— Alan Bennett", "“Imagination is more important than knowledge.  For knowledge is limited, whereas imagination embraces the entire world, stimulating progress, giving birth to evolution.”\n" +
            "— Albert Einstein", "“Where is the ‘any’ key?”\n" +
            "— Homer Simpson”", "“The Internet?  We are not interested in it.”\n" +
            "— Bill Gates, 1993"],
            candidateInfo: undefined,
            snackbar: {
                visible: false,
                message: "",
            },
            toDeposit: 0,
        }
        this.showSnackBar = this.showSnackBar.bind(this);
        this.depositValue = this.depositValue.bind(this);
        this.onChangeTextValue = this.onChangeTextValue.bind(this);
    }

    async componentDidMount() {
        try {
            const candidateInfo = await ContractUtils.getInstance().getCandidateInfo();
            console.log("Candidate Info received is ", candidateInfo);
            this.setState({candidateInfo: candidateInfo});
        } catch (error) {
            console.error(error);
            this.showSnackBar(true, error.toString());
        }
    }

    showSnackBar(visible, message) {
        this.setState({snackbar: {visible, message}})
    }

    depositValue() {
        if (this.state.toDeposit <= 0) {
            this.showSnackBar(true, "Not enough value to deposit");
            return;
        }
        ContractUtils.getInstance().depositValue(this.state.toDeposit).then(() => {
            this.showSnackBar(true, "Deposit done");
            ContractUtils.getInstance().getCandidateInfo()
                .then(candidateInfo => this.setState({candidateInfo: candidateInfo}))
                .catch(error => this.showSnackBar(true, error.toString()));
        }).catch(error => this.showSnackBar(true, error.toString()));
    }

    onChangeTextValue(event) {
        this.setState({toDeposit: event.target.value})
    }

    render() {
        return (
            <Container>
                <Grid container
                      direction="column"
                      alignItems="center"
                      justify="center"
                      spacing={3}>
                        <Avatar
                            style={{
                                width: "10em",
                                height: "10em",
                                justifyContent: "center"
                            }}
                            aria-label="recipe">
                            <img alt="" style={{width: "10em", height: "10em"}}
                                 src={`icons/avatar-${this.state.randomIndex}.gif`}/>
                        </Avatar>

                    <Grid item xs={4}/>
                    <Grid item xs={4}>
                        <Typography className="digitalFont" style={{color: theme.palette.text.primary}} variant="p">
                           {this.state.candidateInfo} Wei
                        </Typography>
                    </Grid>
                    <Grid item xs={4}/>

                    <Grid item xs={4}/>
                    <Grid item xs={4}>
                        <TextField id="outlined-basic"
                                   label="Souls"
                                   variant="outlined"
                                   value={this.state.toDeposit}
                                   onChange={this.onChangeTextValue}
                        />
                    </Grid>
                    <Grid item xs={4}/>

                    <Grid item xs={12}>
                        <Button
                            color="primary"
                            size="large"
                            variant="outlined"
                            startIcon={<AccountBalanceWallet />}
                            onClick={this.depositValue}
                        > Deposit </Button>
                    </Grid>

                </Grid>
                <Snackbar
                    open={this.state.snackbar.visible}
                    autoHideDuration={6000}
                    message={this.state.snackbar.message}
                    action={
                        <Button color="inherit" size="small" onClick={() => this.showSnackBar(false, "")}>
                            Go it
                        </Button>
                    }
                />
            </Container>
        );
    }
}
export default CandidateViewComponent;