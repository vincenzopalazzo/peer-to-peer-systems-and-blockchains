import React from "react";
import {Box, Button, Container, Fab, Grid, Snackbar, Typography} from "@material-ui/core";
import theme from "../utils/Theme";
import { Progress } from 'react-sweet-progress';
import "react-sweet-progress/lib/style.css";
import "../App.css"
import CandidatesView from "./CondidatesView.component";
import ContractUtils from "../utils/ContractUtils";
import AppModel from "../utils/AppModel";
import {ACCOUNT_VOTE, ENVELOP_OPENED, QUORUM_COND, VOTERS_COND} from "../utils/Constant";
import {Navigation} from "@material-ui/icons";

class Home extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            quorum: 1000,
            voters: 0,
            candidates: [],
            snackbar: {
                visible: false,
                message: "",
            },
        }
        this.showSnackBar = this.showSnackBar.bind(this);
        this.openEnvelop = this.openEnvelop.bind(this);
    }

    showSnackBar(visible, message) {
        this.setState({snackbar: {visible, message}})
    }

    openEnvelop() {
        ContractUtils.getInstance().openEnvelope().catch(error => this.showSnackBar(true, error.toString()));
    }

    async componentDidMount() {
        console.log("Init component");
        await ContractUtils.getInstance().initWeb3();
        ContractUtils.getInstance().getCandidateList().then((result) => {
            console.log(`getCandidateList: Return ${result}`);
            this.setState({candidates: result});
        }).catch((error) => this.showSnackBar(true, error.toString()));
        ContractUtils.getInstance().getConditionQuorum().then(async (result) => {
            const constrains = await ContractUtils.getInstance().getConditionQuorum();
            AppModel.getInstance().putValue(QUORUM_COND, constrains[0]);
            AppModel.getInstance().putValue(VOTERS_COND, constrains[1]);
            AppModel.getInstance().putValue(ENVELOP_OPENED, constrains[2]);
            console.error("Envelop opened are ", constrains[2]);
            console.log(`getConditionQuorum: Return ${result[0]}`);
            this.setState({quorum: result[0], voters: result[1]});
        }).catch((error) => this.showSnackBar(true, error.toString()));

        ContractUtils.getInstance().subscribeToEnvelopeCastEvent(() => {
            ContractUtils.getInstance().getConditionQuorum().then((result) => {
                console.log(`getConditionQuorum: Return ${result[0]}`);
                this.setState({quorum: result[0], voters: result[1]});
            }).catch((error) => this.showSnackBar(true, error.toString()));
        });

        let hasVote = await ContractUtils.getInstance().accountHasVote();
        console.log("The account has vote? ", hasVote);
        if (hasVote)
            AppModel.getInstance().putValue(ACCOUNT_VOTE, true)
        ContractUtils.getInstance().subscribeToNewMayorEvent((_) =>
            this.showSnackBar(true, "New Mayor elected")
        );
        ContractUtils.getInstance().subscribeToSayonaraEvent((data) => {
                this.showSnackBar(true, "No Mayor elected");
                AppModel.getInstance().addEvent("New Mayor", data);
            }
        );
        ContractUtils.getInstance().subscribeToEnvelopeOpenEvent((_) =>
            this.showSnackBar(true, "Envelope Opened")
        );
    }

    render() {
        return <>
        <Container>
            <Grid container justify="center" spacing={3}>
                <Grid item xs={12}/>
                <Grid item xs={12}/>
                <Grid item xs={12}/>
            </Grid>

            <Grid container justify="center" spacing={3}>
                <Grid item xs={4}>
                    <Typography className="digitalFont" style={{color: theme.palette.text.primary}} variant="h6">
                        Quorum: {this.state.quorum}
                    </Typography>
                </Grid>
                <Grid item xs={4}/>
                <Grid item xs={4}>
                    <Typography className="digitalFont" style={{color: theme.palette.text.primary}} variant="h6">
                        Voters: {this.state.voters}
                    </Typography>
                </Grid>
            </Grid>

            <Grid item xs={12}/>
            <Grid item xs={12}>
                <Box m={8} >
                    <Progress
                        percent={(this.state.voters / this.state.quorum) * 100}
                        theme={{
                            success: {
                                symbol: '🏄‍',
                                color: '#ab47bc'
                            },
                            active: {
                                symbol: '😀',
                                color: '#ab47bc'
                            },
                            default: {
                                symbol: '😱',
                                color: '#ab47bc'
                            }
                        }}
                    />
                </Box>
            </Grid>
            <CandidatesView
                snackbar={this.showSnackBar}
                candidates={this.state.candidates}
                voters={this.state.voters}
                quorum={this.state.quorum}
            />
        </Container>
            <Snackbar
                open={this.state.snackbar.visible}
                autoHideDuration={6000}
                message={this.state.snackbar.message}
                action={
                    <Button color="inherit" size="small" onClick={() => this.showSnackBar(false, "")}>
                        Go it
                    </Button>
                }
            />

            <Fab variant="extended" style={{position: 'absolute',
                background: theme.palette.primary.main,
                bottom: theme.spacing(2),
                right: theme.spacing(2),}}
                disabled={this.state.quorum !== this.state.voters}
                 onClick={this.openEnvelop}
            >
                <Navigation />
                Open Envelop
            </Fab>
        </>
    }
}

export default Home;