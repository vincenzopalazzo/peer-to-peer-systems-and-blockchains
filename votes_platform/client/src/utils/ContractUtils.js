import Web3 from "web3";
import Mayor from "../contracts/Mayor.json";
import AppModel from "./AppModel";

let _SINGLETON;

class ContractUtils {

    static getInstance() {
        if (_SINGLETON === undefined) {
            _SINGLETON = new ContractUtils();
        }
        return _SINGLETON;
    }

    async initWeb3() {
        if (this.web3)
            return this;
        console.log("Calling initWeb3");
        try {
            console.log("Pre getWeb3");
            this.web3 = await this.getWeb3();
            console.log("Web3 ", this.web3);
            this.network = await this.web3.eth.net.getId();
            console.log("Network id: ", this.network);
            this.contract = new this.web3.eth.Contract(
                Mayor.abi,
                Mayor.networks[this.network].address,
            );
            console.log("web3 init finished");
            this.account = await this.web3.eth.getAccounts();
            this.account = this.account[0];
            console.log(`wallet account is: ${this.account}`);
            AppModel.getInstance().addEvent("Init Wallet", false);
            return this;
        } catch (e) {
            console.log(e);
            throw e;
        }
    }

    async getCandidateList() {
        if (!this.contract) return;
        console.log("Calling getCandidateList");
        console.log(this.contract);
        return await this.contract.methods.get_candidates().call();
    }

    async getConditionQuorum() {
        if (!this.contract) throw Error("App not ready yet");
        console.log("Calling getCandidateList");
        console.log(this.contract);
        return await this.contract.methods.get_condition().call();
    }

    async depositValue(value) {
        if (!this.contract) throw Error("App not ready yet");
        await this.contract.methods.deposit_eth_candidate().send({from: this.account, value: value});
        AppModel.getInstance().addEvent(`Deposit value from ${this.account}`, false);
    }

    async openEnvelope() {
        if (!this.contract) throw Error("App not ready yet");
        if (!this.candidateChoosed) throw Error("candidateChoosed not defined");
        AppModel.getInstance().addEvent(`Open Envelope from ${this.account}`, false);
        return await this.contract.methods.open_envelope(this.sigil, this.candidateChoosed).send({from: this.account, value: this.sould});
    }

    async sendVote(addressToVote, valueToPut) {
        if (!this.contract) throw Error("App not ready yet");
        this.sigil = this.hash(this.account);
        this.sould = valueToPut;
        this.candidateChoosed = addressToVote;
        console.log(`Address to vote is ${addressToVote}`);
        this.envelop = await this.contract.methods.compute_envelope(this.sigil, addressToVote, valueToPut).call();
        console.log(`Envelop computed is ${this.envelop}`);
        AppModel.getInstance().addEvent(`Send Vote from ${this.account}`, false);
        return await this.contract.methods.cast_envelope(this.envelop).send({from: this.account, value: 0});
    }

    async getCandidateInfo() {
        if (!this.contract) throw Error("App not ready yet");
        return await this.contract.methods.get_candidate_soul(this.account).call();
    }

    getAccount() { return this.account }

    async isCandidate() {
        const candidates = await this.getCandidateList();
        const index = candidates.indexOf(this.account);
        return index >= 0;
    }

    async accountHasVote() {
        if (!this.contract) throw Error("App not ready yet");
        return await this.contract.methods.check_has_voted(this.account).call();
    }

    async mayorOrSayonara() {
        if (!this.contract) throw Error("App not ready yet");
        return await this.contract.methods.mayor_or_sayonara().send({from: this.account, value: 0});
    }

    subscribeToEnvelopeCastEvent(callback) {
        if (!this.contract) throw Error("App not ready yet");
        this.contract.events.EnvelopeCast({}, callback).on("data", (event) => {
            console.log("Event Envelope Cast with the following result: ", event);
            AppModel.getInstance().addEvent(`Enevelope Casted`, true)
        });
    }

    subscribeToNewMayorEvent(callback) {
        if (!this.contract) throw Error("App not ready yet");
        this.contract.events.NewMayor({}, callback).on("data", (event) => {
            console.log("Event New Mayor with the following result: ", event);
            AppModel.getInstance().addEvent(`NewMayor Found`, true)
        });
    }

    subscribeToSayonaraEvent(callback) {
        if (!this.contract) throw Error("App not ready yet");
        this.contract.events.Sayonara({}, callback).on("data", (event) => {
            console.log("Event Sayonara with the following result: ", event);
            AppModel.getInstance().addEvent(`No mayor (Sayonara) Found`, true);
        });
    }

    subscribeToNewDepositEvent(callback) {
        if (!this.contract) throw Error("App not ready yet");
        this.contract.events.NewDeposit({}, callback).on("data", (event) => {
            console.log("Event New Deposit with the following result: ", event.toString());
            AppModel.getInstance().addEvent(`New Deposit`, true);
        });
    }

    subscribeToEnvelopeOpenEvent(callback) {
        if (!this.contract) throw Error("App not ready yet");
        this.contract.events.EnvelopeOpen({}, callback).on("data", (event) => {
            console.log("Event Evelope Open with the following result: ", event.toString());
            AppModel.getInstance().addEvent(`Envelope Opened`, true);
        });
    }

    hash(s) {
        /* Simple hash function. */
        let a = 1, c = 0, h, o;
        if (s) {
            a = 0;
            /*jshint plusplus:false bitwise:false*/
            for (h = s.length - 1; h >= 0; h--) {
                o = s.charCodeAt(h);
                a = (a<<6&268435455) + o + (o<<14);
                c = a & 266338304;
                a = c!==0?a^c>>21:a;
            }
        }
        return a;
    };

    getWeb3() {
        return new Promise(async (resolve, reject) => {
            // Modern dapp browsers...
            console.log("DApp enabled in web browsers");
            if (window.ethereum) {
                const web3 = new Web3(window.ethereum);
                try {
                    console.log("Local wallet found.");
                    // Request account access if needed
                    await window.ethereum.enable();
                    // Acccounts now exposed
                    resolve(web3);
                } catch (error) {
                    reject(error);
                }
            } else if (window.web3) {
                // Use Mist/MetaMask's provider.
                const web3 = window.web3;
                console.log("Injected web3 detected.");
                resolve(web3);
            } else {
                const provider = new Web3.providers.HttpProvider(
                    "http://127.0.0.1:8545"
                );
                const web3 = new Web3(provider);
                console.log("No web3 instance injected, using Local web3.");
                resolve(web3);
            }
        });
    }
}

export default ContractUtils;