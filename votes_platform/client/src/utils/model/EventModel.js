class EventModel {
    constructor(name, inputOrOutput) {
        this.name = name;
        this.inputOrOutput = inputOrOutput;
    }

    toString() {
        return this.name.toString();
    }
}

export default EventModel;