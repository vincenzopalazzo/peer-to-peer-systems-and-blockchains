import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#ab47bc',
        },
        secondary: {
            main: '#34324a',
        },
        background: {
            default: '#292d3e',
            paper: '#34324a',
        },
        text: {
            primary: '#676E95',
        },
        divider: '#009688',
    },
});

export default theme
