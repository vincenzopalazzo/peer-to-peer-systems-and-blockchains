/**
 * MyLocalTimes is an mobile app for consulting the local time of the cities.
 * Copyright (C) 2020 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
import EventModel from "./model/EventModel";
import {EVENT_LIST} from "./Constant";

let _SINGLETON;

class AppModel {
    static getInstance() {
        if (_SINGLETON === undefined) {
            _SINGLETON = new AppModel();
        }
        return _SINGLETON;
    }

    constructor() {
        this.mapModel = new Map();
    }

    putValue(key, value) {
        this.mapModel.set(key, value);
    }

    getValue(key) {
        return this.mapModel.get(key);
    }

    getValue(key, def) {
        let val = this.mapModel.get(key);
        if (!val)
            return def;
        return val;
    }

    addEvent(name, inOrOut) {
        const event = new EventModel(name, inOrOut);
        console.log("New invent: ", event.toString());
        let events = this.getValue(EVENT_LIST, new Map());
        events.set(event.name, event);
        this.putValue(EVENT_LIST, events);
    }
}

export default AppModel;